function Errors() {
    this.showError = function(elem, errorMessage) {
        elem.className = "error-border";
        var container = elem.parentNode;
        container.className = 'flex-block error';
        var msgElem = document.createElement('span');
        msgElem.className = "error-message";
        msgElem.innerHTML = errorMessage;
        container.appendChild(msgElem);
    }

    this.resetError = function(elem) {
        var container = elem.parentNode;
        container.className = '';
        if (container.lastChild.className == "error-message") {
            container.removeChild(container.lastChild);
        }
        elem.className = "valid-border";
    }
}
var ERRORS = new Errors();