function TMPL_Menu () {
    this.listOfGoodsTmplTitle = '<tr><td>Товар</td><td>Склад</td><td>Стеллаж</td><td>Ячейка</td><td></td></tr>';
    this.listOfGoodsTmplData = '<tr><td><ins>{{=id}}</ins></td><td>{{=storage}}</td><td>{{=rack}}</td><td>{{=cell}}</td><td><button id="{{=idDel}}" class="del-button" onclick="LOGIC.deleteGood(this)">X</button></td></tr>';
    this.modalSuccessDelTmpl = '<div><h2 class="center-text-modal">Удаление</h2><p>Место свободно:</p><p>Склад - {{=storage}}</p><p>Стеллаж - {{=rack}}</p><p>Ячейка - {{=cell}}</p><div class="center-text-modal"><a href="menu.html" class="main-button">OK</a></div></div>';
}
var TMPL = new TMPL_Menu ();

function TEST_Data () {
    this.forRender = '{"status":200,"responseText":[{"id":"100","storage":"теплый","rack":"25","cell":"2"},{"id":"101","storage":"холодный","rack":"2","cell":"28"},{"id":"102","storage":"теплый","rack":"25","cell":"2"},{"id":"103","storage":"холодный","rack":"9","cell":"2"},{"id":"104","storage":"теплый","rack":"25","cell":"2"}]}';
    this.forDel = '{"status":200,"responseText":{"id":100,"storage":"теплый","rack":25,"cell":2}}';
}
var TEST_DATA = new TEST_Data ();

function VIEW_Menu () {
    this.renderListOfGoods = function (tmplTitle, tmplData, listOfGoods) {
        var listContainer = document.getElementById('list-container');
        var items = tmplTitle;
        for (var i = 0; i < listOfGoods.length; i++) {
            items += renderTemplate(tmplData, { id: listOfGoods[i].id, storage: listOfGoods[i].storage, rack: listOfGoods[i].rack, cell: listOfGoods[i].cell, idDel: listOfGoods[i].id });
        }
        listContainer.innerHTML = items;
    }
    this.renderSuccessDelModal = function (tmplModal, data) {
        var modalContainer = document.getElementById('success-deletion-modal');
        var message = renderTemplate(tmplModal, { storage: data.storage, rack: data.rack, cell: data.cell });
        modalContainer.innerHTML = message;
    }
}
var VIEW = new VIEW_Menu ();

function API_Menu () {
    this.getData = function () {
        var result;
        var xhr = new XMLHttpRequest();
        xhr.open('GET', URL + '?getListOfGoods', false);
        xhr.send();
        return xhr;
    }

    this.deleteGoodFromDB = function (id) {
        var result;
        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', URL + '?id=' + id, false);
        xhr.send();
        return xhr;
    }
}
var API = new API_Menu ();

function LOGIC_Menu () {
    this.init = function () {
        // var data = API.getData("js/_test_data_list_of_goods.json");
        var data = TEST_DATA.forRender;
        var listOfGoods = JSON.parse(data);
        if (listOfGoods.status != 200) {
            location.href = "#failed-deletion-modal"
        }
        else VIEW.renderListOfGoods(TMPL.listOfGoodsTmplTitle, TMPL.listOfGoodsTmplData, listOfGoods.responseText);
    }

    this.deleteGood = function (element) {
        var idOfGood = element.id;
        // var response = API.deleteGoodFromDB(idOfGood);
        var response = TEST_DATA.forDel;
        var infoAboutDel = JSON.parse(response);
        if (infoAboutDel.status != 200) {
            location.href = "#failed-deletion-modal"
        }
        else {
            VIEW.renderSuccessDelModal(TMPL.modalSuccessDelTmpl, infoAboutDel.responseText);
            location.href = '#success-deletion-modal'
        }
    }
}
window.onload = function () {
    var LOGIC = new LOGIC_Menu ();
    LOGIC.init();
}
var LOGIC = new LOGIC_Menu ();
