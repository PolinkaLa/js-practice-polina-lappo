function VALIDATER_Register() {
    this.equalField = function(fieldOne, fieldTwo) {
        return (fieldOne === fieldTwo)
    }
    this.checkPasswords = function(form) {
        var elems = form.elements;
        ERRORS.resetError(elems.password);
        ERRORS.resetError(elems.repassword);
        var fieldsEqual = VALIDATE.equalField(elems.password.value, elems.repassword.value);
        if (!fieldsEqual) { 
            ERRORS.showError(elems.password, ' Пароли не совпадают.');
            ERRORS.showError(elems.repassword, ' Пароли не совпадают.');
        }
    }
    this.hasSpace = function (value) {
        return (value.indexOf(' ') <= 0)
    }

    this.checkLogin = function (form) {
        var elems = form.elements;
        ERRORS.resetError(elems.login);
        if (!VALIDATE.hasSpace(elems.login.value)) {
            ERRORS.showError(elems.login, 'Логин не может содержать пробелов');
        }
    }
}
var VALIDATE = new VALIDATER_Register();

function API_Register () {
    this.sentСredencial = function (data) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", URL + '?' + data, false);
        xhr.send();
        return xhr;
    }
}
var API = new API_Register ();

function LOGIC_Register () {
    this.registrate = function (form) {
        var elems = form.elements;
        var params = 'login=' + elems.login.value + '&password=' + elems.password.value;
        // var response = API.sentСredencial(params);
        var response = '{"status":200}';
        var infoAboutAuth = JSON.parse(response);
        if (infoAboutAuth.status != 200) {
            location.href = "#failed-registration-modal"
        }
        else {
            location.href = '#success-registration-modal'
        }
    }
}
var LOGIC = new LOGIC_Register();