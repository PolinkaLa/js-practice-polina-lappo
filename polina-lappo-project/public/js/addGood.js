function TMPL_Add () {
    this.modalSuccessAddTmpl = '<div><h2 class="center-text-modal">Добавление</h2><p>Место хранения:</p><p>Склад - {{=storage}}</p><p>Стеллаж - {{=rack}}</p><p>Ячейка - {{=cell}}</p><p>Товар - {{=id}}</p><div class="center-text-modal"><a href="menu.html" class="main-button">OK</a></div></div>>';
}
var TMPL = new TMPL_Add ();

function TEST_Data () {
    this.forAdd = '{"status":200,"responseText":{"id":100,"storage":"теплый","rack":25,"cell":2}}';
}
var TEST_DATA = new TEST_Data ();

function VIEW_Add () {
    this.renderSuccessAddModal = function (tmplModal, data) {
        var modalContainer = document.getElementById('success-addition-modal');
        var message = renderTemplate(tmplModal, { storage: data.storage, rack: data.rack, cell: data.cell, id: data.id });
        modalContainer.innerHTML = message;
    }
}
var VIEW = new VIEW_Add ();

function API_Add () {
    this.sentInfo = function (data) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", URL + '?' + data, false);
        xhr.send();
        return xhr;
    }
}
var API = new API_Add ();

function LOGIC_Add () {
    this.put = function (form) {
        var elems = form.elements;
        var leng_th = elems.leng_th.value;
        var wid_th = elems.wid_th.value;
        var heig_ht = elems.heig_ht.value;
        var weig_ht = elems.weig_ht.value;
        var store_type = elems.store_type.value;
        var good_type = elems.good_type.value;
        var params = 'leng_th=' + leng_th + '&wid_th=' + wid_th + '&heig_ht=' + heig_ht + '&weig_ht=' + weig_ht + '&store_type=' + store_type + '&good_type=' + good_type;
        // var response = API.sentInfo(params);
        var response = TEST_DATA.forAdd;
        var infoAboutAdd = JSON.parse(response);
        if (infoAboutAdd.status != 200) {
            location.href = "#failed-addition-modal"
        }
        else {
            VIEW.renderSuccessAddModal(TMPL.modalSuccessAddTmpl, infoAboutAdd.responseText);
            location.href = '#success-addition-modal'
        }
    }
}
var LOGIC = new LOGIC_Add ();
