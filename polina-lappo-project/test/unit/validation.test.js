describe("validation equal field. negative test", function() {    
    it("compares two fields", function() { 
        var input_1 = 123;
        var input_2 = 345;            
        var output = false;
        expect(validation.equalField(input_1, input_2) == output);
    });
});

describe("validation equal field. positive test", function() {    
    it("compares two fields", function() { 
        var input_1 = 123;
        var input_2 = 123;            
        var output = true;
        expect(validation.equalField(input_1, input_2) == output);
    });
});

describe("validation is number. negative test", function() {    
    it("checks whether a value is a number", function() { 
        var input = "dsfdf";            
        var output = false;
        expect(validation.isNumber(input) == output);
    });
});

describe("validation is number. positive test", function() {    
    it("checks whether a value is a number", function() { 
        var input = 1;            
        var output = true;
        expect(validation.isNumber(input) == output);
    });
});

describe("has space. negative test", function() {    
    it("checks whether a value has a space", function() { 
        var input = "jjsdf dsjfhjds djskfhd";            
        var output = true;
        expect(validation.hasSpace(input) == output);
    });
});

describe("has space. positive test", function() {    
    it("checks whether a value has a space", function() { 
        var input = "jjsdfdsjfhjdsdjskfhd";            
        var output = false;
        expect(validation.hasSpace(input) == output);
    });
});