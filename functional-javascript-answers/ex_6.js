function countWords(inputWords) {
    return inputWords.reduce(function(result, word) {
        result[word] = word in result ? result[word] + 1 : 1;
        return result
    }, {})
}
module.exports = countWords