function TMPL_Add () {
    this.modalSuccessAddTmpl = '<div><h2 class="center-text-modal">Добавление</h2><p>Место хранения:</p><p>Склад - {{=storage}}</p><p>Стеллаж - {{=rack}}</p><p>Ячейка - {{=cell}}</p><p>Товар - {{=number}}</p><div class="center-text-modal"><a href="menu.html" class="main-button">OK</a></div></div>';
}
var TMPL = new TMPL_Add ();

function VIEW_Add () {
    this.renderSuccessAddModal = function (tmplModal, data) {
        var modalContainer = document.getElementById('success-addition-modal');
        var message = renderTemplate(tmplModal, { storage: data[0].storage, rack: data[0].rack, cell: data[0].cell, number: data[0].number });
        modalContainer.innerHTML = message;
    }
}
var VIEW = new VIEW_Add ();

function API_Add () {
    this.sentInfo = function (data) {
        var URL = 'http://36874:test123@172.22.2.34:8080/o/mobiRest/addGood?';
        var xhr = new XMLHttpRequest();
        xhr.open('GET', URL + data, false);
        xhr.send();
        return xhr;
        // return '{"responseText":{"number":100,"storage":"теплый","rack":25,"cell":2}}';
    }
}
var API = new API_Add ();

function LOGIC_Add () {
    this.put = function () {
        //var elems = form.elements;
        var leng = document.getElementById('leng');
        var leng_th = leng.value;
        var wid = document.getElementById('wid');
        var wid_th = wid.value;
        var heig = document.getElementById('heig');
        var heig_ht = heig.value;
        var weig = document.getElementById('weig');
        var weig_ht = weig.value;
        var store = document.getElementById('store');
        var store_type = store.value;
        var type = document.getElementById('type');
        var good_type = type.value;
        var params = 'length=' + leng_th + '&width='+ wid_th + '&height=' + heig_ht + '&weight=' + weig_ht + '&store_type=' + store_type + '&good_type=' + good_type + '&user_id=36874';
        // alert(params);
        var response = API.sentInfo(params);
        // console.log(response);
        var infoAboutAdd = JSON.parse(response.responseText);
        // var infoAboutAdd = JSON.parse(response);
        console.log(infoAboutAdd);
        // alert(infoAboutAdd);
        // if ( 201 < 200) {
        if (response.status != 200) {
            location.href = "#failed-addition-modal";
        }
        else {            
            VIEW.renderSuccessAddModal(TMPL.modalSuccessAddTmpl, infoAboutAdd.responseText);
            location.href = '#success-addition-modal';
        }
    }
}
var LOGIC = new LOGIC_Add ();