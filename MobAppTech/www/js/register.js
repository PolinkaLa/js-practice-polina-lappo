function VALIDATER_Register() {
    this.equalField = function(fieldOne, fieldTwo) {
        return (fieldOne === fieldTwo)
    }
    this.checkPasswords = function(form) {
        var elems = form.elements;
        ERRORS.resetError(elems.password);
        ERRORS.resetError(elems.repassword);
        var fieldsEqual = VALIDATE.equalField(elems.password.value, elems.repassword.value);
        if (!fieldsEqual) { 
            ERRORS.showError(elems.password, ' Пароли не совпадают.');
            ERRORS.showError(elems.repassword, ' Пароли не совпадают.');
        }
    }
    this.hasSpace = function (value) {
        return (value.indexOf(' ') <= 0)
    }

    this.checkLogin = function (form) {
        var elems = form.elements;
        ERRORS.resetError(elems.login);
        if (!VALIDATE.hasSpace(elems.login.value)) {
            ERRORS.showError(elems.login, 'Логин не может содержать пробелов');
        }
    }
}
var VALIDATE = new VALIDATER_Register();

function API_Register () {
    this.sentСredencial = function (data) {
        var URL = 'http://36874:test123@192.168.0.102:8080/o/mobiRest/register?';
        var xhr = new XMLHttpRequest();
        xhr.open("GET", URL + data, false);
        xhr.send();
        return xhr;
        // return '{"status":200}';
    }
}
var API = new API_Register ();

function LOGIC_Register () {
    this.registrate = function (form) {
        var elems = form.elements;
        var params = 'screenName=' + elems.screenName.value + '&email=' + elems.email.value + '&firstName=' + elems.firstName.value + '&lastName=' + elems.lastName.value + '&password=' + elems.password.value;
        alert(params);
        var response = API.sentСredencial(params);
        var infoAboutAuth = JSON.parse(response);
        if (infoAboutAuth.status != 200) {
            location.href = "#failed-registration-modal"
        }
        else {
            location.href = '#success-registration-modal'
        }
    }
}
var LOGIC = new LOGIC_Register();