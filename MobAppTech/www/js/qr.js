function READ_QR() {
    this.read = function () {
        cordova.plugins.barcodeScanner.scan(
            function (result) {
                var dataQR = result.text;
                var credencials = dataQR.split(' ');
                var params = {"login": credencials[0],
                            "password": credencials[1]
                };
                LOGIC.login(params);
            },
            function (error) {
                alert("Scanning failed: " + error);
            },
            {
                preferFrontCamera: false,
                showFlipCameraButton: true,
                showTorchButton: true,
                torchOn: false,
                prompt: "Сфокусируйте камеру на QR-коде",
                resultDisplayDuration: 500, // 
                formats: "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
                orientation: "portrait" // (portrait|landscape)
            }
        );
    }
}
var QR = new READ_QR();