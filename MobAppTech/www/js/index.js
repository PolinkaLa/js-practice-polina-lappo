function VALIDATE_Authorization () {

    this.validateEmail = function (form) {
        var elems = form.elements;
        ERRORS.resetError(elems.email);
        var regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!regExp.test(elems.email.value)) {
            ERRORS.showError(elems.email, 'email имеет неверный формат');
        }
    }
}
var VALIDATE = new VALIDATE_Authorization ();

function API_Authorization () {
    this.sentСredencial = function (data) {
        // var URL = 'http://36874:test123@192.168.0.102:8080/o/mobiRest/authorize?';
        // var xhr = new XMLHttpRequest();
        // xhr.open("GET", URL + '?' + data, false);
        // xhr.send();
        // return xhr;
        return '{"status":200}';
    }
}
var API = new API_Authorization ();

function LOGIC_Authorization () {
    this.getUserСredencial = function (form) {
        var elems = form.elements;
        var params = 'email=' + elems.email.value + '&password=' + elems.password.value;
        //alert(params);
        return params;

    }
    this.login = function (params) {
        var response = API.sentСredencial(params);
        var infoAboutAuth = JSON.parse(response);
        if (infoAboutAuth.status != 200) {
        //if (response.status != 200) {
            location.href = "#failed-authorization-modal"
        }
        else {
            location.href = '#success-authorization-modal'
        }
    }
}
var LOGIC = new LOGIC_Authorization ();