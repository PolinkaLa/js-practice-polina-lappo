function TMPL_Menu () {
    this.listOfGoodsTmplData = '<tr><td>{{=id}}</td><td>{{=storage}}</td><td>{{=rack}}/{{=cell}}</td><td><button id="{{=idDel}}" class="del-button" onclick="LOGIC.deleteGood(this)">X</button></td></tr>';
    this.modalSuccessDelTmpl = '<div><h2 class="center-text-modal">Удаление</h2><p>Место свободно:</p><p>Склад - {{=storage}}</p><p>Стеллаж - {{=rack}}</p><p>Ячейка - {{=cell}}</p><div class="center-text-modal"><a href="menu.html" class="main-button">OK</a></div></div>';
}
var TMPL = new TMPL_Menu ();

function VIEW_Menu () {
    this.renderListOfGoods = function (tmplData, listOfGoods) {
        var listContainer = document.getElementById('list-container');
        var items = '';
        for (var i = 0; i < listOfGoods.length; i++) {
            items += renderTemplate(tmplData, { id: listOfGoods[i].id, storage: listOfGoods[i].storage, rack: listOfGoods[i].rack, cell:listOfGoods[i].cell, idDel: listOfGoods[i].id });
        }
        listContainer.innerHTML = items;
    }
    this.renderSuccessDelModal = function (tmplModal, data) {
        var modalContainer = document.getElementById('success-deletion-modal');
        var message = renderTemplate(tmplModal, { storage: data[0].storage, rack: data[0].rack, cell: data[0].cell });
        modalContainer.innerHTML = message;
    }
}
var VIEW = new VIEW_Menu ();

function API_Menu () {
    this.getData = function () {
        var URL = 'http://36874:test123@172.22.2.34:8080/o/mobiRest/getAllGood';
        var xhr = new XMLHttpRequest();
        xhr.open('GET', URL, false);
        xhr.send();
        return xhr;
        // return '{"responseText":[{"rack":1,"id":37801,"storage":"С‚РµРїР»С‹Р№","cell":5},{"rack":1,"id":39002,"storage":"С‚РµРїР»С‹Р№","cell":6}],"status":200}'
        // '{"status":200,"responseText":[{"id":"10000","storage":"теплый","rack":"250","cell":"20"},{"id":"101","storage":"холодный","rack":"2","cell":"28"},{"id":"102","storage":"теплый","rack":"25","cell":"2"},{"id":"103","storage":"холодный","rack":"9","cell":"2"},{"id":"104","storage":"теплый","rack":"25","cell":"2"}, {"id":"100","storage":"теплый","rack":"25","cell":"2"},{"id":"101","storage":"холодный","rack":"2","cell":"28"},{"id":"102","storage":"теплый","rack":"25","cell":"2"},{"id":"103","storage":"холодный","rack":"9","cell":"2"},{"id":"104","storage":"теплый","rack":"25","cell":"2"}]}';
    }

    this.deleteGoodFromDB = function (data) {
        var URL = 'http://36874:test123@172.22.2.34:8080/o/mobiRest/deleteGood?';
        var xhr = new XMLHttpRequest();
        xhr.open('POST', URL + data, false);
        xhr.send();
        return xhr;
        // return '{"responseText":{"id":100,"storage":"теплый","rack":25,"cell":2}}';
    }
}
var API = new API_Menu ();

function LOGIC_Menu () {
    this.init = function () {
        var data = API.getData();
        //alert(data);
        var listOfGoods = JSON.parse(data.responseText);
        console.log(listOfGoods);
        if (data.status != 200) {
            location.href = "#failed-deletion-modal"
        }
        else VIEW.renderListOfGoods(TMPL.listOfGoodsTmplData, listOfGoods.responseText);
    }

    this.deleteGood = function (element) {
        var params = 'goodId=' + element.id + '&user_id=36874';
        // alert(params);
        var response = API.deleteGoodFromDB(params);
        var infoAboutDel = JSON.parse(response.responseText);
        if (response.status != 200) {
            location.href = "#failed-deletion-modal"
        }
        else {
            VIEW.renderSuccessDelModal(TMPL.modalSuccessDelTmpl, infoAboutDel.responseText);
            location.href = '#success-deletion-modal'
        }
    }
}
window.onload = function () {
    var LOGIC = new LOGIC_Menu ();
    LOGIC.init();
}
var LOGIC = new LOGIC_Menu ();
